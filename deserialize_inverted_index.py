#!/usr/bin/env python3


def deserialize_inverted_index(index_path):
    inverted_index = dict()
    index_str = None
    with open(index_path, 'r') as fd:
        index_str = fd.read()
    for entry in index_str.split('\n'):
        entry = entry.split(';')
        inverted_index[entry[0]] = set(entry[1:])

    return inverted_index

inverted_index = deserialize_inverted_index('./corpus/blogs_inverted_index.csv')

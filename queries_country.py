def query_or(index, factors):
    factors = [f.lower() for f in factors]
    result = None
    for factor in factors:
        if result is None:
            result = index.get(factor, set())
        else:
            result = result.union(index.get(factor, set()))
    return result


def query_and(index, factors):
    factors = [f.lower() for f in factors]
    result = None
    for factor in factors:
        if result is None:
            result = index.get(factor, set())
        else:
            result = result.intersection(index.get(factor, set()))
        if len(result) == 0:
            return set()
    return result

def query_multiword(index, mw):
    mw = mw.lower()
    results = set()
    candidates = query_and(index, mw.split())
    for candidate in candidates:
        with open(f'./corpus/country_preformatted/{candidate}', 'r') as fd:
            if mw in fd.read():
                results.add(candidate)
    return results

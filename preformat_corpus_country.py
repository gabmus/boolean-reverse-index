#!/usr/bin/env python3

from os import listdir, makedirs
from os.path import isdir
import argument_parser
from job_dispatcher import dispatch
import json
from nltk.tokenize import TweetTokenizer

# import re
# NON_ALPHA_REGEX = re.compile('[\W_]+')

NON_ALPHA_REMOVAL_TABLE = dict.fromkeys(
    map(
        ord,
        '!@#$%^&*()-_=+[]{};:",.<>/?\\`'
    ),
    None
)

CORPUS_PATH = './corpus/country_dataset.json'
OUT_PATH = './corpus/country_preformatted/'
if not isdir(OUT_PATH):
    makedirs(OUT_PATH)

docs = None
with open(CORPUS_PATH, 'r') as fd:
    docs = json.loads(fd.read())['docs']

CORPUS_SIZE = len(docs)


def preformat(documents_list):
    done = 0
    errors = list()
    for doc_id, document in enumerate(documents_list):
        doc_id = str(doc_id)
        try:
            doc_str = document
            # doc_str = ' '.join(tokenizer(doc_str))
            # doc_str = NON_ALPHA_REGEX.sub('', doc_str)
            doc_str = doc_str.translate(NON_ALPHA_REMOVAL_TABLE).lower().replace("'s", '')
            print
            with open(OUT_PATH+'/'+doc_id, 'w') as fd:
                fd.write(doc_str)
        except Exception:
            errors.append(doc_id)
        if not argument_parser.quiet and not argument_parser.parallel:
            done += 1
            percent_done = int((done/CORPUS_SIZE) * 100)
            print(f'[{"#"*percent_done}{" "*(100-percent_done)}]  {done}/{CORPUS_SIZE}     ', end='\r')

    if not argument_parser.quiet:
        sep = '\n        '
        print(f'\nErrors: {sep.join(errors)}')


if argument_parser.parallel:
    dispatch(preformat, docs)
else:
    preformat(docs)

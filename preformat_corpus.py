#!/usr/bin/env python3

from os import listdir, makedirs
from os.path import isdir
import argument_parser
from job_dispatcher import dispatch
from nltk.tokenize import TweetTokenizer

# import re
# NON_ALPHA_REGEX = re.compile('[\W_]+')

NON_ALPHA_REMOVAL_TABLE = dict.fromkeys(
    map(
        ord,
        '!@#$%^&*()-_=+[]{};:",.<>/?\\`'
    ),
    None
)

CORPUS_PATH = './corpus/blogs/'
OUT_PATH = './corpus/blogs_preformatted/'
if not isdir(OUT_PATH):
    makedirs(OUT_PATH)


def blog_to_dict_l(blog: str):
    dict_l = list()
    while True:
        d = dict()
        next_date_start = blog.find('<date>')
        if next_date_start == -1:
            return dict_l
        next_date_end = blog.find('</date>')
        date = blog[next_date_start+6:next_date_end]
        next_post_start = blog.find('<post>')
        next_post_end = blog.find('</post>')
        post = blog[next_post_start+6:next_post_end].strip()
        d['date'] = date
        d['post'] = post
        dict_l.append(d)
        blog = blog[next_post_end+7:]


CORPUS_SIZE = len(listdir(CORPUS_PATH))


def preformat(documents_list):
    done = 0
    errors = list()
    for document in documents_list:
        try:
            doc_dict_l = None
            with open(CORPUS_PATH+'/'+document, 'r') as fd:
                doc_dict_l = blog_to_dict_l(fd.read())
            doc_str = '\n'.join(
                [d['post'] for d in doc_dict_l]
            )
            # doc_str = ' '.join(tokenizer(doc_str))
            # doc_str = NON_ALPHA_REGEX.sub('', doc_str)
            doc_str = doc_str.translate(NON_ALPHA_REMOVAL_TABLE).lower().replace("'s", '')
            with open(OUT_PATH+'/'+document.split('.')[0], 'w') as fd:
                fd.write(doc_str)
        except Exception:
            errors.append(document)
        if not argument_parser.quiet and not argument_parser.parallel:
            done += 1
            percent_done = int((done/CORPUS_SIZE) * 100)
            print(f'[{"#"*percent_done}{" "*(100-percent_done)}]  {done}/{CORPUS_SIZE}     ', end='\r')

    if not argument_parser.quiet:
        sep = '\n        '
        print(f'\nErrors: {sep.join(errors)}')


if argument_parser.parallel:
    dispatch(preformat, listdir(CORPUS_PATH))
else:
    preformat(listdir(CORPUS_PATH))

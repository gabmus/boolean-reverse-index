#!/usr/bin/env python3

import json
from queries_country import query_and, query_multiword
from deserialize_inverted_index_country import inverted_index


def precision(results, control):
    if len(results) == 0:
        if len(control) == 0:
            return 1.0
        else:
            return 0.0
    results = set([str(r).lower() for r in results])
    control = set([str(c).lower() for c in control])
    correct = set([item for item in results if item in control])
    return len(correct)/len(results)


def recall(results, control):
    if len(results) == 0:
        if len(control) == 0:
            return 1.0
        else:
            return 0.0
    results = set([str(r).lower() for r in results])
    control = set([str(c).lower() for c in control])
    correct = set([item for item in results if item in control])
    return len(correct)/len(control)


ground_truths = None
with open('./corpus/country_dataset.json', 'r') as fd:
    ground_truths = json.loads(fd.read())['queries']

result_csv = 'query;precision;recall;armonic_avg\n'

for term in ground_truths.keys():
    myquery_result = None
    if ' ' in term:
        myquery_result = query_multiword(inverted_index, term)
    else:
        myquery_result = query_and(inverted_index, [term])
    correct_result = ground_truths[term]
    mprecision = precision(myquery_result, correct_result)
    mrecall = recall(myquery_result, correct_result)
    result_csv += f'{term};{mprecision};{mrecall};{(2*mprecision*mrecall)/(mprecision+mrecall) if mprecision+mrecall > 0 else "0"}\n'

with open('./corpus/quality_evaluation_country.csv', 'w') as fd:
    fd.write(result_csv)

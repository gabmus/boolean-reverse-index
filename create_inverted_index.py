#!/usr/bin/env python3

from os import listdir
import argument_parser
# from job_dispatcher import dispatch

CORPUS_PATH = './corpus/blogs_preformatted/'
CORPUS_SIZE = len(listdir(CORPUS_PATH))


def create_inverted_index():
    inverted_index = dict()
    done = 0
    for document in listdir(CORPUS_PATH):
        words = None
        with open(CORPUS_PATH+'/'+document, 'r') as fd:
            words = fd.read().split()
        for word in words:
            if word not in inverted_index.keys():
                inverted_index[word] = set()
            inverted_index[word].add(document)
        if not argument_parser.quiet:
            done += 1
            percent_done = int((done/CORPUS_SIZE) * 100)
            print(f'[{"#"*percent_done}{" "*(100-percent_done)}]  {done}/{CORPUS_SIZE}     ', end='\r')

    if not argument_parser.quiet:
        print('\n')
    return inverted_index


def serialize_inverted_index(index):
    index_str = ''
    for word in index.keys():
        index_str += word + ';' + ';'.join(index[word]) + '\n'
    return index_str


with open('./corpus/blogs_inverted_index.csv', 'w') as fd:
    fd.write(
        serialize_inverted_index(create_inverted_index())
    )

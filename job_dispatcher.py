from multiprocessing import Process, cpu_count


def dispatch(func, args, jobs=cpu_count()+1):
    args_per_job = int(len(args)/jobs)

    processes = list()
    for i in range(0, jobs):
        processes.append(
            Process(
                target=func,
                args=(
                    args[i*args_per_job:i*args_per_job+args_per_job]
                    if i != jobs-1 else args[i*args_per_job:],
                )
            )
        )
    for process in processes:
        process.start()
    for process in processes:
        process.join()

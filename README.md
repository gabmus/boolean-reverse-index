# Boolean Reverse Index

You can download the corpus from here:

- [blog corpus](https://u.cs.biu.ac.il/~koppel/blogs/blogs.zip) (extract in `./corpus/blogs/`)
- [`country_dataset.json`](https://island.ricerca.di.unimi.it/~alfio/shared/country_dataset.json) (put in `./corpus/country_dataset.json`)

# Benchmarks

```
time ./preformat_corpus.py -q
./preformat_corpus.py -q  27.81s user 1.16s system 99% cpu 29.027 total

time ./create_inverted_index.py -q
./create_inverted_index.py -q  32.75s user 0.84s system 98% cpu 33.973 total

time ./deserialize_inverted_index.py
./deserialize_inverted_index.py  14.05s user 1.09s system 99% cpu 15.157 total

# parallel on 9 processes; CPU: i7-6700k

time ./preformat_corpus.py -q -p
./preformat_corpus.py -q -p  46.99s user 1.74s system 611% cpu 7.970 total
```

# Known issues

- Without proper tokenization, stuff like `Suriname-(the Guianas)` becomes `surinamethe` and `guianas`
- Again because of lack of proper tokenization, plurals aren't handled

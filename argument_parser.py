import sys

args = sys.argv[1:]
quiet = '--quiet' in args or '-q' in args
if '--help' in args or '-h' in args:
    print('''Options:
\t-h, --help\t\tShow this help page
\t-q, --quiet\t\tDon't print out anything
\t-p, --parallel\t\tUse multiple processes''')
    exit(0)
parallel = '--parallel' in args or '-p' in args
